package fi.purtonen.auth.repository;

import org.springframework.data.repository.CrudRepository;

import fi.purtonen.auth.model.User;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends CrudRepository<User, Integer>
{
	Optional<User> findByUuid(UUID uuid);
	Optional<User> findByFirstName(String lastName);
}
