package fi.purtonen.auth.model;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class User
{

	@Id
	@Column(columnDefinition = "BINARY(16)")
	private UUID uuid;

	@NotNull
	private String firstName, lastName;

	public User()
	{
		this.uuid = UUID.randomUUID();
	}

	public User(String firstName, String lastName)
	{
		this.uuid = UUID.randomUUID();
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public User(UUID uuid, String firstName, String lastName)
	{
		this.uuid = uuid;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public UUID getUuid()
	{
		return uuid;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setUuid(UUID uuid)
	{
		this.uuid = uuid;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	@Override
	public String toString()
	{
		return "User{" + "uuid=" + uuid + ", firstName=" + firstName + ", lastName=" + lastName + '}';
	}

}
