package fi.purtonen.auth.model;

import java.nio.charset.Charset;
import java.util.Random;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.crypto.password.PasswordEncoder;

@Entity
public class Client
{

	@Id
	@Column(columnDefinition = "BINARY(16)")
	private UUID uuid;

	@NotNull
	private String name, secretHash;

	@Transient
	private String secretString;
	
	@Autowired
	@Transient
	private PasswordEncoder passwordEncoder;

	public Client()
	{
	}

	public Client(String name)
	{
		
		this.uuid = UUID.randomUUID();
		this.name = name;

		// Create random secret string
		byte[] secretBytes = new byte[64];
		new Random().nextBytes(secretBytes);
		String secretString = new String(secretBytes, Charset.forName("UTF-8"));

		this.secretString = secretString;
		this.secretHash = this.passwordEncoder.encode(secretString);
	}

	public UUID getUuid()
	{
		return uuid;
	}

	public String getName()
	{
		return name;
	}

	public String getSecretHash()
	{
		return secretHash;
	}

	public String getSecretString()
	{
		return secretString;
	}

}
