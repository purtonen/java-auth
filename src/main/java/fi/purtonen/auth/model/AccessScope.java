package fi.purtonen.auth.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class AccessScope
{

	@Id
	private String scope;
	
	public AccessScope(){}
	
	public AccessScope(String scope){
		this.scope = scope;
	}
}
