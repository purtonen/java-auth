package fi.purtonen.auth.controller;

import fi.purtonen.auth.model.User;
import fi.purtonen.auth.repository.UserRepository;

import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class UserController
{

	@Autowired
	private UserRepository userRepo;

	@GetMapping("/users")
	public Iterable<User> getAll()
	{
		return userRepo.findAll();
	}

	@GetMapping("/users/{uuid}")
	public ResponseEntity getOne(@PathVariable UUID uuid)
	{
		Optional<User> user = userRepo.findByUuid(uuid);
		if (!user.isPresent()) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity(user.get(), HttpStatus.OK);
	}

	@PostMapping("/users")
	public ResponseEntity create(@Valid @RequestBody(required = true) User user)
	{
		userRepo.save(user);
		return new ResponseEntity(user, HttpStatus.CREATED);
	}

	@DeleteMapping("/users/{uuid}")
	public ResponseEntity delete(@PathVariable(required = true) UUID uuid)
	{
		Optional<User> user = userRepo.findByUuid(uuid);
		if (!user.isPresent()) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		userRepo.delete(user.get());
		return new ResponseEntity(null, HttpStatus.NO_CONTENT);
	}

}
